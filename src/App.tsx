import { RouterProvider, createBrowserRouter } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/login/Login";
import Register from "./pages/register/Register";
import { ProtectedRoute, isLogin } from "./helper/ProtectedRoute";
import User from "./pages/user/User";
import AddUserForm from "./pages/user/AddUserForm";
import EditUserForm from "./pages/user/EditUserForm";
import Post from "./pages/post";
import AddPostForm from "./pages/post/AddPostForm";
import Navbar from "./pages/navbar/Navbar";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
    loader: ProtectedRoute,
  },
  {
    path: "/login",
    element: <Login />,
    loader: isLogin,
  },
  {
    path: "/register",
    element: <Register />,
    loader: ProtectedRoute,
  },
  {
    path: "/user",
    element: <User />,
    loader: ProtectedRoute,
  },
  {
    path: "/user/addUser",
    element: <AddUserForm />,
    loader: ProtectedRoute,
  },
  {
    path: "/user/editUser/:id",
    element: <EditUserForm />,
    loader: ProtectedRoute,
  },
  {
    path: "post",
    element: <Post />,
    loader: ProtectedRoute,
  },
  {
    path: "post/addPost",
    element: <AddPostForm />,
    loader: ProtectedRoute,
  },
  {
    path: "*",
    element: <h1>404 - Not found</h1>,
  },
]);

function App() {
  console.log("Test: ", router);
  return (
    <>
      {/* <Navbar /> */}
      <RouterProvider router={router} />
    </>
  );
}

export default App;
