import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { IUser } from "../user/type";
import axios from "axios";
import { BASE_URL, user_endpoint } from "../../helper/api";
import FormControl from "@mui/material/FormControl";
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Container,
  Input,
  InputLabel,
  TextField,
  Typography,
} from "@mui/material";
import "../login/style.css";

const Login = () => {
  const navigate = useNavigate();
  const [name, setname] = useState("");
  const [password, setPassword] = useState("");
  const [userList, setUserList] = useState<IUser[]>([]);

  useEffect(() => {
    fetchUserData();
  }, []);

  const fetchUserData = async () => {
    try {
      const response = await axios.get(`${BASE_URL}/${user_endpoint}`);
      setUserList(response.data);
    } catch (error) {
      alert(error);
    }
  };

  // Handler
  const handleLogin = () => {
    const user = userList.find(
      (user) => user.name === name && user.password === password
    );
    const token = {
      name: name,
      email: user?.email,
      password: password,
      role: user?.role,
      _id: user?._id,
    };

    console.log("TOken: ", token);
    const isAuthenticated = userList
      .map((item) => JSON.stringify(item))
      .includes(JSON.stringify(token));
    console.log(
      "MAP: ",
      userList.map((item) => JSON.stringify(item))
    );
    console.log(
      "INCLUDEs: ",
      userList
        .map((item) => JSON.stringify(item))
        .includes(JSON.stringify(token))
    );
    console.log("JSON.stringify: ", JSON.stringify(token));
    if (isAuthenticated) {
      localStorage.setItem("Token", JSON.stringify(token));
      navigate("/", { replace: true }); // replace: true = this to prevent getting back to login page after you login
    } else {
      alert("Invalid Account");
    }
  };

  return (
    <div>
      {/* <Card sx={{ minWidth: 500, minHeight: 500 }}>
        <CardContent>
          <Typography
            variant="h1"
            sx={{ fontSize: 20 }}
            color="text.primary"
            align="center">
            BootCamp Login
          </Typography>
        </CardContent>
        <CardContent>
          <FormControl fullWidth>
            <InputLabel htmlFor="name">Name</InputLabel>
            <Input
              id="name"
              aria-describedby="my-helper-text"
              value={name}
              onChange={(e) => setname(e.target.value)}
            />
          </FormControl>
          <br />
          <br />
          <FormControl fullWidth>
            <InputLabel htmlFor="password">Password</InputLabel>
            <Input
              type="password"
              id="password"
              aria-describedby="my-helper-text"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </FormControl>
          <br />
          <br />
        </CardContent>
        <CardActions>
          <FormControl fullWidth>
            <Button variant="contained" color="primary" onClick={handleLogin}>
              Login
            </Button>
          </FormControl>
        </CardActions>
      </Card> */}
      <Container maxWidth="sm">
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}>
          <Typography variant="h5" gutterBottom>
            BootCamp
          </Typography>
          <Box
            component="form"
            sx={{
              width: "100%",
              marginTop: 1,
              border: "1px solid #ccc",
              borderRadius: "5px",
              padding: "20px",
            }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              autoComplete="username"
              autoFocus
              value={name}
              onChange={(e) => setname(e.target.value)}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <Button
              type="button"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              onClick={handleLogin}>
              Sign In
            </Button>
          </Box>
        </Box>
      </Container>
    </div>
  );
};

export default Login;
