// import TodoForm from "./component/TodoForm";
// import TodoFormFunction from "./component/TodoFormFunction";

import { Typography } from "@mui/material";
import Navbar from "../navbar/Navbar";

const Home = () => {
  const userToken = JSON.parse(localStorage.getItem("Token") || '{"name": ""}');
  return (
    <>
      {/* <TodoFormFunction /> */}
      <Navbar />
      <Typography variant="h5">Welcome {userToken["name"]}!</Typography>
    </>
  );
};

export default Home;
