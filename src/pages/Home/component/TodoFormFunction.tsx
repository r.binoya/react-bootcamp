import { ChangeEvent, useState } from "react";
import TodoDisplay from "./TodoDisplay";
import { todoObj } from "../type";
import { useNavigate } from "react-router-dom";
import Navbar from "../../navbar/Navbar";

const TodoFormFunction = () => {
  const [showData, setShowData] = useState(false);
  const [inputTitle, setInputTitle] = useState("");
  const [inputMessage, setInputMessage] = useState("");
  const [editdata, setEditData] = useState(true);
  const [todos, setTodos] = useState<todoObj[]>([
    { id: 1, title: "Test", message: "Test1" },
    { id: 2, title: "todo1", message: "todo1" },
  ]);
  const [index, setIndex] = useState<number>();
  const navigate = useNavigate();

  // Handler
  const showDataHandler = (event: ChangeEvent<HTMLInputElement>) => {
    const { checked } = event.target;
    setShowData(checked);
  };

  const btnAddHandler = () => {
    const newTodo: todoObj = {
      id: new Date().valueOf(),
      title: inputTitle,
      message: inputMessage,
    };
    setTodos([...todos, newTodo]);
  };

  const handleActionDelete = (id: number) => {
    const newInputList = todos.filter((item) => item.id !== id);
    setTodos(newInputList);
  };

  const handleActionEdit = (id: number) => {
    setEditData(false);
    const toEdit = todos.filter((item) => item.id === id);
    const toEditTitle = toEdit.map((item) => item.title);
    const toEditMessage = toEdit.map((item) => item.message);
    setInputTitle(toEditTitle.toString());
    setInputMessage(toEditMessage.toString());
    setIndex(id);
  };

  const handleActionUpdate = () => {
    const toUpdate = {
      title: inputTitle,
      message: inputMessage,
    };
    const updatedData = todos.map((item) => {
      if (item.id === index) {
        return { ...item, ...toUpdate };
      }
      return item;
    });
    setTodos(updatedData);
    setInputTitle("");
    setInputMessage("");
    setEditData(true);
  };

  const handleBack = () => {
    setInputTitle("");
    setInputMessage("");
    setEditData(true);
  };

  const handleLogout = () => {
    localStorage.removeItem("Token");
    navigate("/login");
  };

  return (
    <>
      <div>
        <Navbar />
        <h1>Home Page</h1>
        <button onClick={handleLogout}>Logout</button>
        <br />
        <br />
        <div>
          <input
            type="text"
            placeholder="Enter Title"
            value={inputTitle}
            onChange={(e) => setInputTitle(e.target.value)}
          />
          <br />
          <br />
          <textarea
            placeholder="Enter Message"
            value={inputMessage}
            onChange={(e) => setInputMessage(e.target.value)}></textarea>
          <br />
          <br />
          {editdata ? (
            <button className="btn" onClick={btnAddHandler}>
              Add
            </button>
          ) : (
            <>
              <button className="btn" onClick={handleActionUpdate}>
                Update
              </button>
              <button className="btn" onClick={handleBack}>
                Back
              </button>
            </>
          )}
          <br />
          <br />
          <label>
            <input
              type="checkbox"
              onChange={showDataHandler}
              checked={showData}
            />
            Show Data
          </label>
          <br />
          {showData && (
            <TodoDisplay
              input_list={todos}
              actionDelete={handleActionDelete}
              actionEdit={handleActionEdit}
            />
          )}
        </div>
      </div>
    </>
  );
};

export default TodoFormFunction;
