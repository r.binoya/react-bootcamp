import React, { ChangeEvent } from "react";
import { IProp, IState, todoObj } from "../type";
import TodoDisplay from "./TodoDisplay";

class TodoForm extends React.Component<IProp, IState> {
  constructor(props: IProp) {
    super(props);
    this.state = {
      inputTitle: "",
      inputMessage: "",
      todos: [
        { id: 1, title: "Test", message: "Test1" },
        { id: 2, title: "todo1", message: "todo1" },
      ],
      showData: false,
      editData: true,
    };
  }
  inputTitleHandler = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    this.setState({ inputTitle: value });
  };

  inputMessageHandler = (event: ChangeEvent<HTMLTextAreaElement>) => {
    const { value } = event.target;
    this.setState({ inputMessage: value });
  };

  btnAddHandler = () => {
    const newTodo: todoObj = {
      id: new Date().valueOf(),
      title: this.state.inputTitle,
      message: this.state.inputMessage,
    }; // initialize new data to be push in array
    this.setState((prevState) => ({
      todos: [...prevState.todos, newTodo], //to push in array
    }));
    this.setState({ inputTitle: "", inputMessage: "" }); // to clear fields
  };

  showDataHandler = (event: ChangeEvent<HTMLInputElement>) => {
    const { checked } = event.target;
    this.setState({ showData: checked });
  };

  handleActionDelete = (id: number) => {
    const { todos } = this.state;
    const newInputList = todos.filter((item) => item.id !== id);
    this.setState({ todos: newInputList });
  };

  handleActionEdit = (id: number) => {
    this.setState({ editData: false });
    const { todos } = this.state;
    const toEdit = todos.filter((item) => item.id === id);
    const toEditTitle = toEdit.map((item) => item.title);
    const toEditMessage = toEdit.map((item) => item.message);
    this.setState({ inputTitle: toEditTitle.toString() });
    this.setState({ inputMessage: toEditMessage.toString() });
    this.setState({ index: id });
  };

  handleActionUpdate = () => {
    const toUpdate = {
      title: this.state.inputTitle,
      message: this.state.inputMessage,
    };
    const updatedData = this.state.todos.map((item) => {
      if (item.id === this.state.index) {
        return { ...item, ...toUpdate };
      }
      return item;
    });
    this.setState({
      todos: updatedData,
      inputTitle: "",
      inputMessage: "",
      editData: true,
    });
  };

  handleBack = () => {
    this.setState({
      editData: true,
      inputTitle: "",
      inputMessage: "",
    });
  };

  render() {
    const { todos, showData, editData } = this.state;
    return (
      <>
        <div>
          <h1>Home Page</h1>
          <div>
            <input
              type="text"
              placeholder="Title"
              onChange={this.inputTitleHandler}
              value={this.state.inputTitle}
            />
            <br />
            <br />
            <textarea
              placeholder="Message"
              onChange={this.inputMessageHandler}
              value={this.state.inputMessage}></textarea>
            <br />
            {editData ? (
              <button className="btn" onClick={this.btnAddHandler}>
                Add
              </button>
            ) : (
              <>
                <button className="btn" onClick={this.handleActionUpdate}>
                  Update
                </button>
                <button className="btn" onClick={this.handleBack}>
                  Back
                </button>
              </>
            )}
            <br />
            <label>
              <input
                type="checkbox"
                onChange={this.showDataHandler}
                checked={showData}
              />
              Show Data
            </label>
            <br />
            {showData && (
              <TodoDisplay
                input_list={todos}
                actionDelete={this.handleActionDelete}
                actionEdit={this.handleActionEdit}
              />
            )}
          </div>
        </div>
      </>
    );
  }
}

export default TodoForm;
