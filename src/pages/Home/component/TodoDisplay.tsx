import "../style.css";
import { IDisplayTodo } from "../type";

const TodoDisplay = (props: IDisplayTodo) => {
  const { input_list, actionDelete, actionEdit } = props;
  return (
    <>
      {!input_list.length ? (
        <span>No data</span>
      ) : (
        <>
          <table border={1}>
            <thead>
              <tr>
                <th>Title</th>
                <th>Message</th>
                <th>ID</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {input_list.map((todo) => (
                <tr key={todo.id}>
                  <td>{todo.title}</td>
                  <td>{todo.message}</td>
                  <th>{todo.id}</th>
                  <td>
                    <button className="btn" onClick={() => actionEdit(todo.id)}>
                      Edit
                    </button>
                    <button
                      className="btn"
                      onClick={() => actionDelete(todo.id)}>
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </>
      )}
    </>
  );
};

export default TodoDisplay;
