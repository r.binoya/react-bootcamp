export interface IProp {}

export interface todoObj {
  id: number;
  title: string;
  message: string;
}

export interface IState {
  inputTitle: string;
  inputMessage: string;
  todos: todoObj[];
  showData: boolean;
  editData: boolean;
  index?: number;
}

export interface IDisplayTodo {
  input_list: todoObj[];
  actionDelete: (value: any) => void;
  actionEdit: (value: any) => void;
}
