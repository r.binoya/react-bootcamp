import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { BASE_URL, user_endpoint } from "../../helper/api";

const EditUserForm = () => {
  const navigate = useNavigate();
  const { id } = useParams<{ id: string }>();
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [role, setRole] = useState("");

  useEffect(() => {
    fetchUserData();
  }, [id]);

  const fetchUserData = async () => {
    try {
      const response = await axios.get(`${BASE_URL}/${user_endpoint}/${id}`);
      setName(response.data.name);
      setEmail(response.data.email);
      setPassword(response.data.password);
      setRole(response.data.role);
    } catch (error) {}
  };

  const handleUpdateUser = () => {
    alert(id);
  };

  return (
    <>
      <div>
        <h1>Update User</h1>
        <label htmlFor="name">Name:</label>
        <br />
        <input
          type="text"
          id="name"
          placeholder="Enter Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <br />
        <br />
        <label htmlFor="email">Email:</label>
        <br />
        <input
          type="email"
          id="email"
          placeholder="Enter Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <br />
        <br />
        <label htmlFor="password">Password:</label>
        <br />
        <input
          type="password"
          id="password"
          placeholder="Enter Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <br />
        <br />
        <label htmlFor="role">Role:</label>
        <br />
        <select id="role" onChange={(e) => setRole(e.target.value)}>
          <option value={role} selected>
            {role}
          </option>
          <option value="Admin">Admin</option>
          <option value="Lead">Lead</option>
          <option value="Developer">Developer</option>
        </select>
        <br />
        <br />
        <button className="btn-primary" onClick={handleUpdateUser}>
          Update
        </button>
        <span>|</span>
        <button onClick={() => navigate("/user")}>Back</button>
      </div>
    </>
  );
};

export default EditUserForm;
