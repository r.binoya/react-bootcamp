import {
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import Navbar from "../navbar/Navbar";
import useUser from "./useUser";

const User = () => {
  const { navigate, userList } = useUser();
  return (
    <>
      <Navbar />
      <Typography variant="h5" gutterBottom>
        User List
      </Typography>
      <Button
        variant="contained"
        color="primary"
        onClick={() => navigate("addUser")}>
        Add new User
      </Button>
      <br />
      <br />
      <TableContainer component={Paper} style={{ maxWidth: "xl" }}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Role</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {userList.map((user) => (
              <TableRow key={user._id}>
                <TableCell>{user.name}</TableCell>
                <TableCell>{user.email}</TableCell>
                <TableCell>{user.role}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default User;
