import { useEffect, useState } from "react";
import { IUser } from "./type";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { BASE_URL, user_endpoint } from "../../helper/api";

const useUser = () => {
  const navigate = useNavigate();
  const [userList, setUserList] = useState<IUser[]>([]);

  useEffect(() => {
    fetchUser();
  }, []);

  const fetchUser = async () => {
    try {
      const response = await axios.get(`${BASE_URL}/${user_endpoint}`);
      setUserList(response.data);
    } catch (error) {
      alert(error);
    }
  };

  return {
    navigate,
    userList,
  };
};

export default useUser;
