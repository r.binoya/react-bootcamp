import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { BASE_URL, user_endpoint } from "../../helper/api";
import Navbar from "../navbar/Navbar";
import {
  Box,
  Button,
  Container,
  FormControl,
  InputLabel,
  TextField,
  Typography,
} from "@mui/material";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";

const AddUserForm = () => {
  const navigate = useNavigate();
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [role, setRole] = useState("");

  const handleAddUser = async () => {
    try {
      const newUser = { name, email, password, role };
      await axios.post(`${BASE_URL}/${user_endpoint}`, newUser);
      setName("");
      setEmail("");
      setPassword("");
      setRole("");
      navigate("/user");
      alert("Success: User successfully add.");
    } catch (error) {
      alert(error);
    }
  };

  const handleChange = (event: SelectChangeEvent) => {
    setRole(event.target.value as string);
  };

  console.log(role);

  return (
    <>
      <Navbar />
      <Container maxWidth="sm">
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}>
          <Typography variant="h5" gutterBottom>
            Add New User
          </Typography>
          <Box
            component="form"
            sx={{
              width: "100%",
              marginTop: 1,
              border: "1px solid #ccc",
              borderRadius: "5px",
              padding: "20px",
            }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="name"
              label="Name"
              name="name"
              autoComplete="name"
              autoFocus
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
            <TextField
              margin="normal"
              type="email"
              required
              fullWidth
              id="email"
              label="Email"
              name="email"
              autoComplete="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <TextField
              margin="normal"
              type="password"
              required
              fullWidth
              id="password"
              label="Password"
              name="password"
              autoComplete="current-password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <FormControl fullWidth>
              <InputLabel id="select-Role">Role</InputLabel>
              <Select
                labelId="select-Role"
                id="role"
                value={role}
                label="Role"
                onChange={handleChange}>
                <MenuItem value="ADMIN">Admin</MenuItem>
                <MenuItem value="LEAD">Lead</MenuItem>
                <MenuItem value="DEVELOPER">Developer</MenuItem>
              </Select>
            </FormControl>
            <Button
              type="button"
              variant="contained"
              sx={{ mt: 3, mb: 2, mr: 5 }}
              onClick={handleAddUser}>
              Add
            </Button>
            <Button
              type="button"
              variant="outlined"
              sx={{ mt: 3, mb: 2 }}
              onClick={() => navigate("/user")}>
              Back
            </Button>
          </Box>
        </Box>
      </Container>
    </>
  );
};

export default AddUserForm;
