import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { IPost } from "./type";
import axios from "axios";
import { BASE_URL, post_endpoint, user_endpoint } from "../../helper/api";
import { IUser } from "../user/type";
import EditPostForm from "./EditPostForm";
import Navbar from "../navbar/Navbar";
import {
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

const Post = () => {
  const navigate = useNavigate();
  const [postList, setPostList] = useState<IPost[]>([]);
  const [userList, setUserList] = useState<IUser[]>([]);
  const [editPosts, setEditPosts] = useState({} as IPost);
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [postId, setPostId] = useState("");
  // MUI Modal trigger open and close
  const [open, setOpen] = React.useState(false);

  const handleOpenModal = (id: string) => {
    setOpen(true);
    setPostId(id);
  };

  const handleClose = () => {
    setOpen(false);
  };
  //end of MUI Modal trigger open and close

  useEffect(() => {
    fetchPostData();
    fetchUserData();
  }, []);

  const fetchPostData = async () => {
    try {
      const response = await axios.get(`${BASE_URL}/${post_endpoint}`);
      setPostList(response.data);
    } catch (error) {
      alert(error);
    }
  };

  const fetchUserData = async () => {
    try {
      const response = await axios.get(`${BASE_URL}/${user_endpoint}`);
      setUserList(response.data);
    } catch (error) {
      alert(error);
    }
  };

  // Handler//

  // Delete
  const handleDeletePost = async (id: string) => {
    try {
      await axios.delete(`${BASE_URL}/${post_endpoint}/${id}`);
      fetchPostData();
      setOpen(false);
      navigate("/post");
    } catch (error) {
      alert(error);
    }
  };

  // Edit
  const handleEditPost = (postData: IPost) => {
    setIsEdit(true);
    setEditPosts(postData);
  };

  // Call Back
  const handleShowPostList = () => {
    setIsEdit(false);
    fetchPostData();
  };

  return (
    <>
      <Navbar />
      {isEdit === false ? (
        <div>
          <Typography variant="h5">Post List</Typography>
          <Button
            variant="contained"
            color="primary"
            onClick={() => navigate("addPost")}>
            Add new Post
          </Button>
          <br />
          <br />
          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Title</TableCell>
                  <TableCell>Message</TableCell>
                  <TableCell>Poster</TableCell>
                  <TableCell>Status</TableCell>
                  <TableCell>Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {postList
                  .filter((post) => post.status === "ACTIVE")
                  .map((post) => (
                    <TableRow key={post._id}>
                      <TableCell>{post.title}</TableCell>
                      <TableCell>{post.message}</TableCell>
                      {userList
                        .filter((user) => user._id === post.userId)
                        .map((user) => (
                          <TableCell key={user._id}>{user.name}</TableCell>
                        ))}
                      <TableCell>{post.status}</TableCell>
                      <TableCell>
                        <Button
                          type="button"
                          variant="contained"
                          sx={{ mt: 3, mb: 2, mr: 2 }}
                          onClick={() => handleEditPost(post)}>
                          Edit
                        </Button>
                        <Button
                          type="button"
                          variant="contained"
                          color="error"
                          sx={{ mt: 3, mb: 2 }}
                          onClick={() => handleOpenModal(post._id)}>
                          Delete
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </TableContainer>

          {/* MUI MODAl */}
          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description">
            <DialogTitle id="alert-dialog-title">{"Delete Post"}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                Are you sure you want to delete this post?
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>No</Button>
              <Button onClick={() => handleDeletePost(postId)} autoFocus>
                Yes
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      ) : (
        <EditPostForm postData={editPosts} postCallBack={handleShowPostList} />
      )}
    </>
  );
};

export default Post;
