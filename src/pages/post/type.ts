export interface IPost {
  _id: string;
  title: string;
  message: string;
  userId: string;
  status: string;
}

export type IPostProps = {
  postData: IPost;
  postCallBack: () => void;
};
