import axios from "axios";
import { useState } from "react";
import { BASE_URL, post_endpoint } from "../../helper/api";
import { IPost, IPostProps } from "./type";
import Navbar from "../navbar/Navbar";
import { Box, Button, Container, TextField, Typography } from "@mui/material";

const EditPostForm = (props: IPostProps) => {
  const { postData, postCallBack } = props;
  const [title, setTitle] = useState(postData.title);
  const [message, setMessage] = useState(postData.message);

  // Handler
  const handleUpdatePost = async () => {
    try {
      const userId = JSON.parse(localStorage.getItem("Token") || '{"_id": ""}');
      const updatedPost: IPost = {
        userId: userId["_id"],
        title,
        message,
        status: "ACTIVE",
        _id: postData._id,
      };
      await axios.put(
        `${BASE_URL}/${post_endpoint}/${postData._id}`,
        updatedPost
      );
      setMessage("");
      setTitle("");
      postCallBack();
      alert("Successfully updated.");
    } catch (error) {
      alert(error);
    }
  };

  return (
    <>
      <Navbar />
      <Container maxWidth="sm">
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}>
          <Typography variant="h5" gutterBottom>
            Update Post
          </Typography>
          <Box
            component="form"
            sx={{
              width: "100%",
              marginTop: 1,
              border: "1px solid #ccc",
              borderRadius: "5px",
              padding: "20px",
            }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="title"
              label="Title"
              name="title"
              autoComplete="title"
              autoFocus
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              id="message"
              label="Message"
              name="message"
              autoComplete="message"
              value={message}
              onChange={(e) => setMessage(e.target.value)}
            />
            <Button
              type="button"
              variant="contained"
              sx={{ mt: 3, mb: 2, mr: 5 }}
              onClick={handleUpdatePost}>
              Update
            </Button>
            <Button
              type="button"
              variant="outlined"
              sx={{ mt: 3, mb: 2 }}
              onClick={postCallBack}>
              Back
            </Button>
          </Box>
        </Box>
      </Container>
      {/* <div>
        <h1>Update Post</h1>
        <label htmlFor="title">Title:</label>
        <br />
        <input
          type="text"
          id="title"
          placeholder="Enter Title"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <br />
        <br />
        <label htmlFor="message">Message:</label>
        <br />
        <textarea
          id="message"
          placeholder="Enter Message"
          value={message}
          onChange={(e) => setMessage(e.target.value)}></textarea>
        <br />
        <br />
        <button className="btn-primary" onClick={handleUpdatePost}>
          Update
        </button>
        <span>|</span>
        <button onClick={postCallBack}>Back</button>
      </div> */}
    </>
  );
};

export default EditPostForm;
