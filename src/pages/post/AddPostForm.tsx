import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { BASE_URL, post_endpoint } from "../../helper/api";
import Navbar from "../navbar/Navbar";
import { Box, Button, Container, TextField, Typography } from "@mui/material";

const AddPostForm = () => {
  const navigate = useNavigate();
  const [title, setTitle] = useState("");
  const [message, setMessage] = useState("");

  // Handler
  const handleAddPost = async () => {
    try {
      const userId = JSON.parse(localStorage.getItem("Token") || '{"_id": ""}');
      const newPost = {
        userId: userId["_id"],
        title,
        message,
        status: "ACTIVE",
      };
      await axios.post(`${BASE_URL}/${post_endpoint}`, newPost);
      setTitle("");
      setMessage("");
      navigate("/post");
    } catch (error) {
      alert(error);
    }
  };

  return (
    <>
      <Navbar />
      <Container maxWidth="sm">
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}>
          <Typography variant="h5" gutterBottom>
            Add New Post
          </Typography>
          <Box
            component="form"
            sx={{
              width: "100%",
              marginTop: 1,
              border: "1px solid #ccc",
              borderRadius: "5px",
              padding: "20px",
            }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="title"
              label="Title"
              name="title"
              autoComplete="title"
              autoFocus
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              id="message"
              label="Message"
              name="message"
              autoComplete="message"
              value={message}
              onChange={(e) => setMessage(e.target.value)}
            />
            <Button
              type="button"
              variant="contained"
              sx={{ mt: 3, mb: 2, mr: 5 }}
              onClick={handleAddPost}>
              Add
            </Button>
            <Button
              type="button"
              variant="outlined"
              sx={{ mt: 3, mb: 2 }}
              onClick={() => navigate("/post")}>
              Back
            </Button>
          </Box>
        </Box>
      </Container>
    </>
  );
};

export default AddPostForm;
