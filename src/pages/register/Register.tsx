import { useNavigate } from "react-router-dom";

const Register = () => {
  const navigate = useNavigate();
  const userToken = localStorage.getItem("Token");
  return (
    <div>
      <h1>Register {userToken}</h1>
      <br />
      <input type="text" id="username" placeholder="Enter Full Name" />
      <br />
      <br />
      <input type="text" id="username" placeholder="Enter Username" />
      <br />
      <br />
      <input type="password" placeholder="Enter Password" />
      <br />
      <br />
      <input type="password" placeholder="Match Password" />
      <br />
      <br />
      <button className="btn">Register</button>
      <button onClick={() => navigate("/")}>Back to Home</button>
    </div>
  );
};

export default Register;
